Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  get 'index/index'
  resources :car_model do
  	resources :car_body
  	resources :generation
  	resources :auto_serium
  	resources :drive
  	resources :gear_box
  	resources :fuel
  	resources :wheel_radiu
  	resources :city_and_center
  end
  root 'index#index'
end
