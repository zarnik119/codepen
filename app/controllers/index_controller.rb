class IndexController < ApplicationController
  def index
      @ColorCarBody = ColorCarBody.all
      @ColorCarInterior = ColorCarInterior.all
      @ColorCarRoof = ColorCarRoof.all
      @Generation = Generation.all
      @CarBody = CarBody.all
      @Type = Type.all
      @Drive = Drive.all
      @GearBox = GearBox.all
      @Fuel = Fuel.all
      @CityAndCenter = CityAndCenter.all
      @WheelRadiu = WheelRadiu.all
      @CarModel = CarModel.all
      @Car = Car.all
      @AutoSerium = AutoSerium.all

      def findCarModelCount(car_model)
        return Car.where(:car_model => car_model).count
      end
      def findCarSeriaCount(seria)
        return Car.where(:car_model => CarModel.where(:auto_serium => seria)).count
      end
      def findCarTypeCount(type)
        return Car.where(:type => type).count
      end
      def findCarHistoryCount
        return Car.where(:history => true).count
      end
      def findCarDriveCount(drive)
        return Car.where(:car_model => CarModel.where(:drive => drive)).count
      end
      def findCarGearBoxCount(gear_box)
        return Car.where(:car_model => CarModel.where(:gear_box => gear_box)).count
      end
      def findCarFuelCount(fuel)
        return Car.where(:car_model => CarModel.where(:fuel => fuel)).count
      end
      def findCarCCBCount(ccb)
        return Car.where(:color_car_body => ccb).count
      end
      def findCarCCICount(cci)
        return Car.where(:color_car_interior => cci).count
      end
      def findCarCCRCount(ccr)
        return Car.where(:color_car_roof => ccr).count
      end
  end
end
