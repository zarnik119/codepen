class CarModelController < ApplicationController
  def index
    @CarModel = CarModel.all
  end

  def show
    @CarModel = CarModel.find(params[:id])
  end

  def new
    @CarModel = CarModel.new
  end

  def edit
    @CarModel = CarModel.find(params[:id])
  end

  def create
    @CarModel = CarModel.new(article_params)

    if @CarModel.save
      redirect_to @CarModel
    else
      render 'new'
    end
  end

  def update
    @CarModel = CarModel.find(params[:id])

    if @CarModel.update(article_params)
      redirect_to @CarModel
    else
      render 'edit'
    end
  end

  def destroy
    @CarModel = CarModel.find(params[:id])
    @CarModel.destroy

    redirect_to articles_path
  end

  private
    def article_params
      params.require(:car_model).permit(:name, :photo, :car_body, :generation, :auto_serium, :drive, :gear_box, :fuel, :wheel_radiu, :city_and_center)
    end
end
