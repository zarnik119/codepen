ActiveAdmin.register Car do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :title, :kilometers, :price, :count_owner, :power_horse_strength, :volume, :datw_first_register, :power, :city_and_center, :car_model, :color_car_body, :color_car_interior, :color_car_roof, :history, :type
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

end
