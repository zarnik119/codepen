class Car < ApplicationRecord
  belongs_to :city_and_center
  belongs_to :car_model
  belongs_to :color_car_body
  belongs_to :color_car_interior
  belongs_to :color_car_roof
  belongs_to :history
  belongs_to :type
end