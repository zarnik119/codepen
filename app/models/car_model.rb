class CarModel < ApplicationRecord
  belongs_to :car_body
  belongs_to :generation
  belongs_to :auto_serium
  belongs_to :drive
  belongs_to :gear_box
  belongs_to :fuel
  belongs_to :wheel_radiu
end