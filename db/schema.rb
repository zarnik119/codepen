# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_18_211605) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "auto_seria", force: :cascade do |t|
    t.string "seria"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "car_bodies", force: :cascade do |t|
    t.string "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "car_models", force: :cascade do |t|
    t.string "name"
    t.string "photo"
    t.string "model_year"
    t.bigint "car_body_id"
    t.bigint "generation_id"
    t.bigint "auto_serium_id"
    t.bigint "drive_id"
    t.bigint "gear_box_id"
    t.bigint "fuel_id"
    t.bigint "wheel_radiu_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["auto_serium_id"], name: "index_car_models_on_auto_serium_id"
    t.index ["car_body_id"], name: "index_car_models_on_car_body_id"
    t.index ["drive_id"], name: "index_car_models_on_drive_id"
    t.index ["fuel_id"], name: "index_car_models_on_fuel_id"
    t.index ["gear_box_id"], name: "index_car_models_on_gear_box_id"
    t.index ["generation_id"], name: "index_car_models_on_generation_id"
    t.index ["wheel_radiu_id"], name: "index_car_models_on_wheel_radiu_id"
  end

  create_table "cars", force: :cascade do |t|
    t.string "title"
    t.integer "kilometers"
    t.integer "price"
    t.integer "count_owner"
    t.integer "power_horse_strength"
    t.integer "power"
    t.integer "volume"
    t.integer "date_first_registration"
    t.bigint "city_and_center_id"
    t.bigint "car_model_id"
    t.bigint "color_car_body_id"
    t.bigint "color_car_interior_id"
    t.bigint "color_car_roof_id"
    t.bigint "history_id"
    t.bigint "type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["car_model_id"], name: "index_cars_on_car_model_id"
    t.index ["city_and_center_id"], name: "index_cars_on_city_and_center_id"
    t.index ["color_car_body_id"], name: "index_cars_on_color_car_body_id"
    t.index ["color_car_interior_id"], name: "index_cars_on_color_car_interior_id"
    t.index ["color_car_roof_id"], name: "index_cars_on_color_car_roof_id"
    t.index ["history_id"], name: "index_cars_on_history_id"
    t.index ["type_id"], name: "index_cars_on_type_id"
  end

  create_table "city_and_centers", force: :cascade do |t|
    t.string "city"
    t.string "center"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "color_car_bodies", force: :cascade do |t|
    t.string "color"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "color_car_interiors", force: :cascade do |t|
    t.string "color"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "color_car_roofs", force: :cascade do |t|
    t.string "color"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "drives", force: :cascade do |t|
    t.string "drive_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fuels", force: :cascade do |t|
    t.string "fuel_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gear_boxes", force: :cascade do |t|
    t.string "gearbox_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "generations", force: :cascade do |t|
    t.string "generation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "histories", force: :cascade do |t|
    t.boolean "has_crash"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "types", force: :cascade do |t|
    t.string "auto_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wheel_radius", force: :cascade do |t|
    t.string "radius"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "car_models", "auto_seria"
  add_foreign_key "car_models", "car_bodies"
  add_foreign_key "car_models", "drives", column: "drive_id"
  add_foreign_key "car_models", "fuels"
  add_foreign_key "car_models", "gear_boxes"
  add_foreign_key "car_models", "generations"
  add_foreign_key "car_models", "wheel_radius"
  add_foreign_key "cars", "car_models"
  add_foreign_key "cars", "city_and_centers"
  add_foreign_key "cars", "color_car_bodies"
  add_foreign_key "cars", "color_car_interiors"
  add_foreign_key "cars", "color_car_roofs"
  add_foreign_key "cars", "histories"
  add_foreign_key "cars", "types"
end
