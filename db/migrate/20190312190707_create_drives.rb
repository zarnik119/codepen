class CreateDrives < ActiveRecord::Migration[5.2]
  def change
    create_table :drives do |t|
      t.string :drive_type

      t.timestamps
    end
  end
end
