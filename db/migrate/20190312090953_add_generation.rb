class AddGeneration < ActiveRecord::Migration[5.2]
  def change
  	Generation.create(:generation => "982");
  	Generation.create(:generation => "981");
  	Generation.create(:generation => "987 II");
  	Generation.create(:generation => "987 I");
  	Generation.create(:generation => "986 II");
  	Generation.create(:generation => "986 I");
  	Generation.create(:generation => "992");
  	Generation.create(:generation => "991 II");
  	Generation.create(:generation => "991 I");
  	Generation.create(:generation => "997 II");
  	Generation.create(:generation => "997 I");
  	Generation.create(:generation => "996 II");
  	Generation.create(:generation => "996 I");
  	Generation.create(:generation => "993 II");
  	Generation.create(:generation => "993 I");
  	Generation.create(:generation => "964");
  	Generation.create(:generation => "G-Model II");
  	Generation.create(:generation => "G-Model I");
  	Generation.create(:generation => "F-Model");
  	Generation.create(:generation => "G2");
  	Generation.create(:generation => "G1 II");
  	Generation.create(:generation => "G1");
  	Generation.create(:generation => "Macan 1 II");
  	Generation.create(:generation => "Macan 1");
  	Generation.create(:generation => "E3");
  	Generation.create(:generation => "E2 II");
  	Generation.create(:generation => "E2");
  	Generation.create(:generation => "E1 II");
  	Generation.create(:generation => "E1");
  	Generation.create(:generation => "918");
  	Generation.create(:generation => "Carrera GT");
  end
end
