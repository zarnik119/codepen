class CreateHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :histories do |t|
      t.boolean :has_crash

      t.timestamps
    end
  end
end
