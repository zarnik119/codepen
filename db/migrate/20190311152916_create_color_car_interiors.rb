class CreateColorCarInteriors < ActiveRecord::Migration[5.2]
  def change
    create_table :color_car_interiors do |t|
      t.string :color
      t.string :code

      t.timestamps
    end
  end
end
