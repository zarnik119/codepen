class AddColorCarRoof < ActiveRecord::Migration[5.2]
  def change
  	ColorCarRoof.create(:color => "Черный", :code => "#000000");
  	ColorCarRoof.create(:color => "Синий", :code => "#004b80");
  	ColorCarRoof.create(:color => "Коричневый", :code => "#483f40");
  	ColorCarRoof.create(:color => "Серый", :code => "#333333");
  	ColorCarRoof.create(:color => "Красный", :code => "#fb1a08");
  end
end
