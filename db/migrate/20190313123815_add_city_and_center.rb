class AddCityAndCenter < ActiveRecord::Migration[5.2]
  def change
  	CityAndCenter.create(:city => "Казань", :center => "Borsche center(Казань)");
  	CityAndCenter.create(:city => "Екатеринбург", :center => "Borsche center(Екатеринбург)");
  	CityAndCenter.create(:city => "Минеральные Воды", :center => "Borsche center(Минеральные Воды)");
  	CityAndCenter.create(:city => "Нижний Новгород", :center => "Borsche center(Нижний Новгород)");
  	CityAndCenter.create(:city => "Москва", :center => "Borsche center(Москва)");
  	CityAndCenter.create(:city => "Тольятти", :center => "Borsche center(Тольятти)");
  	CityAndCenter.create(:city => "Иркутск", :center => "Borsche center(Иркутск)");
  	CityAndCenter.create(:city => "Воронеж", :center => "Borsche center(Воронеж)");
  	CityAndCenter.create(:city => "Ижевск", :center => "Borsche center(Ижевск)");
  	CityAndCenter.create(:city => "Краснодар", :center => "Borsche center(Краснодар)");
  end
end
