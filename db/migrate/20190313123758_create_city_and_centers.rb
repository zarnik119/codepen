class CreateCityAndCenters < ActiveRecord::Migration[5.2]
  def change
    create_table :city_and_centers do |t|
      t.string :city
      t.string :center

      t.timestamps
    end
  end
end
