class CreateCarBodies < ActiveRecord::Migration[5.2]
  def change
    create_table :car_bodies do |t|
      t.string :body

      t.timestamps
    end
  end
end
