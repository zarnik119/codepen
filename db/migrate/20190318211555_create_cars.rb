class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :title
      t.integer :kilometers
      t.integer :price
      t.integer :count_owner
      t.integer :power_horse_strength
      t.integer :power
      t.integer :volume
      t.integer :date_first_registration
      t.references :city_and_center, foreign_key: true
      t.references :car_model, foreign_key: true
      t.references :color_car_body, foreign_key: true
      t.references :color_car_interior, foreign_key: true
      t.references :color_car_roof, foreign_key: true
      t.references :history, foreign_key: true
      t.references :type, foreign_key: true

      t.timestamps
    end
  end
end