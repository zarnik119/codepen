class AddType < ActiveRecord::Migration[5.2]
  def change
  	Type.create(:auto_type => "Новые автомобили");
  	Type.create(:auto_type => "Автомобили с пробегом Borsche Approved");
  	Type.create(:auto_type => "Автомобили с пробегом");
  	Type.create(:auto_type => "Classic");
  end
end
