class AddColorCarInterior < ActiveRecord::Migration[5.2]
  def change
  	ColorCarInterior.create(:color => "Черный", :code => "#000000");
  	ColorCarInterior.create(:color => "Синий", :code => "#004b80");
  	ColorCarInterior.create(:color => "Коричневый", :code => "#483f40");
  	ColorCarInterior.create(:color => "Зеленый", :code => "#1c2f2f");
  	ColorCarInterior.create(:color => "Бежевый", :code => "#cc9966");
  	ColorCarInterior.create(:color => "Серый", :code => "#333333");
  	ColorCarInterior.create(:color => "Красный", :code => "#fb1a08");
  end
end
