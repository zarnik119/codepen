class CreateGearBoxes < ActiveRecord::Migration[5.2]
  def change
    create_table :gear_boxes do |t|
      t.string :gearbox_type

      t.timestamps
    end
  end
end
