class AddColorCarBody < ActiveRecord::Migration[5.2]
  def change
  	ColorCarBody.create(:color => "Черный", :code => "#000000");
  	ColorCarBody.create(:color => "Серебристый", :code => "#cccccc");
  	ColorCarBody.create(:color => "Синий", :code => "#004b80");
  	ColorCarBody.create(:color => "Коричневый", :code => "#483f40");
  	ColorCarBody.create(:color => "Зеленый", :code => "#25945c");
  	ColorCarBody.create(:color => "Золотой", :code => "#db883a");
  	ColorCarBody.create(:color => "Бежевый", :code => "#ddc8c1");
  	ColorCarBody.create(:color => "Белый", :code => "#efefef");
  	ColorCarBody.create(:color => "Серый", :code => "#333333");
  	ColorCarBody.create(:color => "Красный", :code => "#fb1a08");
  	ColorCarBody.create(:color => "Желтый", :code => "#ffcc00");
  	ColorCarBody.create(:color => "Фиолетовый", :code => "#2d1746");
  	ColorCarBody.create(:color => "Оранжевый", :code => "#d7361c");
  end
end
