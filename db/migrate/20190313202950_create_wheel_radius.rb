class CreateWheelRadius < ActiveRecord::Migration[5.2]
  def change
    create_table :wheel_radius do |t|
      t.string :radius

      t.timestamps
    end
  end
end
