class CreateColorCarRoofs < ActiveRecord::Migration[5.2]
  def change
    create_table :color_car_roofs do |t|
      t.string :color
      t.string :code

      t.timestamps
    end
  end
end
