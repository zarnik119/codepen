class CreateCarModels < ActiveRecord::Migration[5.2]
  def change
    create_table :car_models do |t|
      t.string :name
      t.string :photo
      t.string :model_year
      t.references :car_body, foreign_key: true
      t.references :generation, foreign_key: true
      t.references :auto_serium, foreign_key: true
      t.references :drive, foreign_key: true
      t.references :gear_box, foreign_key: true
      t.references :fuel, foreign_key: true
      t.references :wheel_radiu, foreign_key: true

      t.timestamps
    end
  end
end
